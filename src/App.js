import React, { useState, useEffect } from 'react'
import { ControlPanel } from './components/ControlPanel'
import { PlayingPanel } from './components/PlayingPanel'
import { getRandomWords } from './utils'

export default () => {
  const [result, setResult] = useState()
  const [answer, setAnswer] = useState()
  const [guess, setGuess] = useState()
  const [correct, setCorrect] = useState()

  const handleNewGame = () => {
    setResult('')
    setAnswer(getRandomWords())
    setGuess('')
    setCorrect()
  }

  const handleGuess = () => {
    setCorrect(answer === guess)
    setResult(guess)
  }

  const game = {
    result,
    answer,
    guess,
    correct,
    setGuess,
    setResult,
    handleNewGame,
    handleGuess
  }

  return (
    <main>
      <PlayingPanel game={game} />
      <ControlPanel game={game} />
    </main>
  )
}
