import React from 'react'

export const Button = (props) => {
  const { children, type = 'blue', ...restProps } = props;

  const styles = ['button', type].join(' ')

  return (<button {...restProps} className={styles}>{children}</button>);
}
