import React, { useState, useEffect } from 'react'
import { Button } from './Button'
import { SplitWords } from './SplitWords'

const ANSWER_SHOW_TIME = 3000

export const ControlPanel = (props) => {
  const { handleNewGame, answer } = props.game
  const [showAnswer, setShowAnswer] = useState(false)

  // hide anwser when new game
  useEffect(() => {
    setShowAnswer(false)
  }, [answer])

  // hide anwser sometimes later when showed
  useEffect(() => {
    const timer = setTimeout(() => {
      setShowAnswer(false)
    }, ANSWER_SHOW_TIME)
    return () => clearTimeout(timer)
  }, [showAnswer])

  return (
    <section className='control-panel block'>
      <h2>New Card</h2>
      <Button onClick={handleNewGame} type='blue'>New Card</Button>
      <SplitWords value={answer} hide={!showAnswer} limit={4} style={{ margin: '15vh 0px' }} />
      <Button disabled={showAnswer || !answer} onClick={() => setShowAnswer(true)} type='pink'>Show Result</Button>
    </section>
  )
}
