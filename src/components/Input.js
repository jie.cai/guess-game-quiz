import React from 'react'

export const Input = (props) => {
  const { value = '', onChange, ...restProps } = props

  const handleChange = event => {
    onChange && onChange(event.target.value.toUpperCase())
  }
  return (<input {...restProps} value={value} onChange={handleChange} className='input' />)
}
