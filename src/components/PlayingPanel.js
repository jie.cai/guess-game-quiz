import React from 'react'
import { Input } from './Input'
import { Button } from './Button'
import { SplitWords } from './SplitWords'

export const PlayingPanel = (props) => {
  const { guess, result, answer, correct, setGuess, setResult, handleGuess } = props.game
  
  return (
    <section className='playing-panel'>
      <section className='result block'>
        <h2>Your Result</h2>
        <SplitWords value={guess} limit={4} />
        {
          result ? correct ?
            <span style={{ color: 'green' }}>SUCCESS</span> :
            <span style={{ color: 'red' }}>FAILD</span> :
            null
        }
      </section>
      <section className='guess block'>
        <h2>Guess Card</h2>
        <Input
          disabled={!answer}
          maxLength={4}
          value={guess}
          onChange={value => {
            setGuess(value)
            setResult()
          }}
          onKeyDown={e => e.key === 'Enter' && handleGuess()}
        />
        <br />
        <Button disabled={!answer} onClick={handleGuess} type='green'>Guess</Button>
      </section>
    </section>
  )
}
