import React from 'react'

export const SplitWords = (props) => {
  const { value = '', limit, hide = false, style } = props
  
  return (
    <div className='split-words' style={style}>
      {
        Array(limit).fill('').map((_, index) => (
          <span key={index}>{value[index] ? (hide ? '*' : value[index]) : ''}</span>
        ))
      }
    </div>
  )
}
