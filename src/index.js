import React from 'react'
import ReactDOM from 'react-dom'
import 'modern-normalize'
import App from './App'
import './index.less'

ReactDOM.render(<App />, document.getElementById('app'))
