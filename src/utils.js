
export const getRandomWords = (length = 4) =>
  Array
    .from({ length: 26 }, (_, i) => String.fromCharCode(65 + i))
    .sort(() => 0.5 - Math.random())
    .slice(0, length)
    .join('')
